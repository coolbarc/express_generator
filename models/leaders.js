const mongoose = require('mongoose');
const Schema = mongoose.Schema;
require('mongoose-currency').loadType(mongoose);
const Currencty = mongoose.Types.Currency;

const leadersSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required:true
    },
    designation: {
        type: String,
        required: true
    },
    abbr: {
        type: String,
        required:true
    },
    description: {
        type: String,
        required:true
    },
    featured: {
        type: Boolean,
        required:true
    }
});

const leaders = mongoose.model('leaders', leadersSchema);
module.exports = leaders;